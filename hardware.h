/***********************************************************************
	ハードウェア制御
------------------------------------------------------------------------

***********************************************************************/
#ifndef _HARDWARE_H_
#include "iodefine.h"
#include "uart.h"

#define LED1	PORTG.PODR.BIT.B7
#define LED2	PORTG.PODR.BIT.B6
#define LED3	PORT6.PODR.BIT.B7
#define LED4	PORT6.PODR.BIT.B8
#define LED5	PORT6.PODR.BIT.B0
#define LED6	PORTG.PODR.BIT.B2
#define LED7	PORT7.PODR.BIT.B0
#define LED8	PORT9.PODR.BIT.B7
#define LED9	PORT4.PODR.BIT.B0
#define LED10	PORT4.PODR.BIT.B1
#define LED11	PORT4.PODR.BIT.B2
#define LED15	PORT7.PODR.BIT.B7
#define LED16	PORT7.PODR.BIT.B6

void Initialize(void);

#define  _HARDWARE_H_
#endif
