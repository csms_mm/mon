#include "mon.h"
#include "charcode.h"

const MON_COMMAND_LIST_TYPE MonCommandList[] =
{
	{ "PI",			MonCmdPI,		"PI {Adr}",					"PortIn"			},
	{ "PO",			MonCmdPO,		"PO {Adr},{Dat}",			"PortOut"			},
	{ "BYTE",		MonCmdByte,		"Byte {flag}",				"Byte Access"		},
	{ "WORD",		MonCmdWord,		"Word {flag}",				"Word Access" 		},
	{ "LONG",		MonCmdLong,		"Long {flag}",				"Long Access"		},
	{ "ASC",		MonCmdAscii,	"Asc {flag}",				"Ascii Enable"		},
	{ "ReadBack",	MonCmdReadBack,	"ReadBack {flag}",			"Read Back"			},
	{ "?",			MonCmdHelp,		"?",						"This Message"		},
	{ null,			MonCmdNop,		null						, null				}
};

MON_SEQ_ENUM MonGetCommand(const MON_COMMAND_LIST_TYPE *CmdList,  LINEBUFF_TYPE *Line);
void MonGetOption(LINEBUFF_TYPE *Line);
long MonGetHexParam(LINEBUFF_TYPE *Line);
bool MonParamIsData(LINEBUFF_TYPE *Line);


//============================================================================
//	オプションをデフォルト設定にする
//----------------------------------------------------------------------------
//
//============================================================================
void LineBuffOptionReset(LINEBUFF_TYPE *Line)
{
	Line->Option.Word					= 0;	//全クリア
	Line->Option.Bit.Loop				= OFF;
	Line->Option.Bit.Esc				= OFF;
	Line->Option.Bit.ReadBack			= ON;
	Line->Option.Bit.Asc				= OFF;
	Line->Option.Bit.DispAdr			= ON;
	Line->Option.Bit.ByteSize			= 2;
}

//============================================================================
//	対応する関数のCCmdList番号を返す
//----------------------------------------------------------------------------
//
//============================================================================
void LineBuffClear(LINEBUFF_TYPE *Line)
{
	Line->Key					= -1;
	Line->Pos 					= 0;
	Line->Adr					= 0;
	Line->Data					= 0;
	Line->Count					= 0;
	Line->AdrAdd				= 0;
	Line->ActAdr				= 0;
	Line->DataAdd				= 0;
	Line->ActData				= 0;
	Line->WaitCount				= 0;
	Line->Seq					= MON_INIT;
	Line->WaitCount				= 0;
	LineBuffOptionReset(Line);
}

//============================================================================
//	対応する関数のCCmdList番号を返す
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonGetCommand(const MON_COMMAND_LIST_TYPE *CmdList,  LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM	iResult = MON_INIT;
	char			Name[16];
	int				Length;
	int 			n, i;
	char    		*Inp_p;
	const char		*Cmd_p;
	bool			flag;
	
	Inp_p			= Line->LineBuff;
	
	//最初の空白を削除
	while (*Inp_p == ' ') Inp_p ++;
	
	if (*Inp_p == 0)
	{
		iResult 			= MON_RUN;
		Line->Func 			= MonCmdNop;
	}
	else
	{
		//入力文字を大文字に変換
		for ( n = 0; n < sizeof(Name); n ++, Inp_p ++)
		{
			if ((*Inp_p <= ' ') || (*Inp_p == '/')||(*Inp_p == 0))
			{
				Name[n]  	= 0;
				Length		= n;
				break;
			}
			else if ((*Inp_p >= 'a') && (*Inp_p <= 'z')) 
			{
				Name[n] = *Inp_p - 'a' + 'A';
			}
			else
			{
				Name[n] = *Inp_p;
			}
		}

		//区切りを読み飛ばす
		while (*Inp_p == ' ') Inp_p ++;
		Line->Cmd			= Inp_p;
		
		for ( n = 0; CmdList[n].Name != null; n ++)
		{
			Inp_p 			= Name;
			Cmd_p 			= CmdList[n].Name;
			flag			= true;
			for (i = 0; i < Length; i ++)
			{
				if (*Inp_p != *Cmd_p) 
				{
					flag 			= false;
					break;
				}
				Inp_p ++;
				Cmd_p ++;
			}
			//コマンド文字がまだ残っている
			if (*Cmd_p != 0) flag = false;
			
			//コマンド一致
			if (flag) 
			{
				Line->Func 		= CmdList[n].Func;
				Line->Seq		= MON_INIT;
				iResult			= MON_RUN;
				break;
			}
		}
	}
	//return MonCmdNop;
	return(iResult);
	
}

//============================================================================
//	オプションを取得
//----------------------------------------------------------------------------
//
//============================================================================
void MonGetOption(LINEBUFF_TYPE *Line)
{
	char		c;
	char		*Cmd;
	bool		Flag = false;

	for (Cmd = Line->Cmd; *Cmd != 0; Cmd ++)
	{
		if (Flag)
		{
			c = *Cmd;
			if ((c >= 'a') && (c <= 'z'))
			{
				c = c - 'a' + 'A';
			}
			switch(c)
			{
				case 'A':		//ASCII 表示
					Line->Option.Bit.Asc 		= ON;
					break;
				case 'C':		//連続動作
					Line->Option.Bit.Loop 		= ON;
					break;
				case 'B':		//バイトアクセス
					Line->Option.Bit.ByteSize 	= 1;
					break;
				case 'W':		//ワードアクセス
					Line->Option.Bit.ByteSize 	= 2;
					break;
				case 'L':		//ロングアクセス
					Line->Option.Bit.ByteSize 	= 4;
					break;
				case 'R':		//リードバック
					Line->Option.Bit.ReadBack	= ON;
					break;
			}
			Flag = false;
		}
		else
		{
			if (*Cmd == '/') 
			{
				Flag = true;
			}
		}
	}
}

//============================================================================
//	パラメータ値を取得
//----------------------------------------------------------------------------
//
//============================================================================
long MonGetHexParam(LINEBUFF_TYPE *Line)
{
	long iResult				= 0;
	char *Param;
	long n;
	
	while (*Line->Cmd==' ') Line->Cmd ++;

	for (Param = Line->Cmd; *Param != 0; Param ++)
	{
		if ((*Param >= '0') && (*Param <= '9'))
		{
			n = *Param - '0';
		}
		else if ((*Param >= 'A') && (*Param <= 'F'))
		{
			n = *Param - 'A' + 10;
		}
		else if ((*Param >= 'a') && (*Param <= 'f'))
		{
			n = *Param - 'a' + 10;
		}
		else
		{
			break;
		}
		iResult = (iResult << 4) | n;
	}
	while 	(*Param==' ') Param ++;
	if 		(*Param==',') Param ++;	

	Line->Cmd = Param;
	return(iResult);
}

//============================================================================
//	パラメータが数字か
//----------------------------------------------------------------------------
//	true		数値データ
//	false		数値データはない
//============================================================================
bool MonParamIsData(LINEBUFF_TYPE *Line)
{
	bool bResult   		= false;
	
	while (*Line->Cmd==' ') Line->Cmd ++;
	
	if ((*Line->Cmd>='A') && (*Line->Cmd<='F')) bResult = true;
	if ((*Line->Cmd>='a') && (*Line->Cmd<='f')) bResult = true;
	if ((*Line->Cmd>='0') && (*Line->Cmd<='9')) bResult = true;
	
	return(bResult);
}

//============================================================================
//	データ出力	
//----------------------------------------------------------------------------
//
//============================================================================
void MonSendData(UART_TYPE *uart, LINEBUFF_TYPE *Line, long Data)
{
	//char	cpBuff[9];
	//int  	iColSize = Line->Option.Bit.ByteSize * 2;
	
	if (Line->Option.Bit.DispAdr)
	{
		MonSendHex(uart, Line->Adr, 8);
		UartSendByte(uart, ':');
	}
	MonSendHex(uart, Data, Line->Option.Bit.ByteSize * 2);
	
	/*
	for (iCol = 0; iCol < iColSize; iCol ++)
	{
		n = Data & 0x0f;
		if ( n < 10) 
		{
			c = '0' + n;
		}
		else
		{
			c = 'A' + n - 10;
		}
		cpBuff[iColSize - iCol - 1]  = c;	
		Data = Data >> 4;
	}
	cpBuff[iColSize] = 0;
	UartSendText(uart, cpBuff);
	*/
}

void MonSendHex(UART_TYPE *uart, long Data, int iColSize)
{
	char	cpBuff[9];
	int		iCol;
	int		n;
	char	c;

	for (iCol = 0; iCol < iColSize; iCol ++)
	{
		n = Data & 0x0f;
		if ( n < 10) 
		{
			c = '0' + n;
		}
		else
		{
			c = 'A' + n - 10;
		}
		cpBuff[iColSize - iCol - 1]  = c;	
		Data = Data >> 4;
	}
	cpBuff[iColSize] = 0;
	UartSendText(uart, cpBuff);
}




//============================================================================
//	ポート入力	
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdPI(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	bool		bSuccess = true;
	MON_SEQ_ENUM iResult = MON_INIT;

	//long		Adr;
	long		Data;
	
	if (Line->Key==ccESC)
	{
		Line->Option.Bit.Loop = false;
	}

	switch(Line->Seq)
	{
		case MON_INIT:
			if (bSuccess) bSuccess 		= MonParamIsData(Line);
			if (bSuccess) Line->Adr		= MonGetHexParam(Line);	
			if (bSuccess) MonGetOption(Line);
			if (bSuccess)
			{
				Line->Seq			= MON_RUN;
			}
			else
			{
				iResult				= MON_INIT;		
				UartSendText(uart, "usage PI {adr}\r\n");
			}
		case MON_RUN:
			break;
		default:
			break;
	}

	if (bSuccess)
	{
		Data		= *((long *)Line->Adr);
		MonSendData(uart, Line, Data);
		if (Line->Option.Bit.Loop)
		{
			UartSendText(uart, "\r");
			iResult				= MON_RUN;		
		}
		else
		{
			UartSendText(uart, CRLF);
			iResult				= MON_INIT;		
		}
	}
	return(iResult);
}

//============================================================================
//	ポート出力
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdPO(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	bool		bSuccess = true;
	long		Data;
	MON_SEQ_ENUM iResult = MON_INIT;
	
	switch(Line->Seq)
	{
	case MON_INIT:
		if (bSuccess) bSuccess 		= MonParamIsData(Line);
		if (bSuccess) Line->Adr		= MonGetHexParam(Line);	
		if (bSuccess) Line->Data	= MonGetHexParam(Line);	
		if (bSuccess) MonGetOption(Line);
		if (bSuccess)
		{
			switch(Line->Option.Bit.ByteSize)
			{
			case 1:
				*((char *)Line->Adr)	= (char)Line->Data;	
				break;
			case 2:
				*((short *)Line->Adr)	= (short)Line->Data;	
				break;
			case 4:
				*((long *)Line->Adr)	= (long)Line->Data;	
				break;
			}
			if (Line->Option.Bit.ReadBack)
			{
				iResult			= MON_RUN;
				Line->Seq		= MON_OPERATION;
			}
		}		
		else
		{
			iResult				= MON_INIT;		
			UartSendText(uart, "usage PO {adr},{data}\r\n");
		}
		break;
	case MON_RUN:	//read back
		Data		= *((long *)Line->Adr);
		MonSendData(uart, Line, Data);
		iResult			= MON_INIT;
		break;		
	}
	return(iResult);
}

//============================================================================
//	バイトアクセス標準
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdByte(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM iResult = MON_INIT;

	Line->Option.Bit.ByteSize		= 1;
	UartSendText(uart, "Byte Access Ready\r\n");
	return(iResult);
}
//============================================================================
//	ワードアクセス標準
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdWord(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM iResult = MON_INIT;

	Line->Option.Bit.ByteSize		= 2;
	UartSendText(uart, "Word Access Ready\r\n");
	return(iResult);
}
//============================================================================
//	ロングアクセス標準
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdLong(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM iResult = MON_INIT;

	Line->Option.Bit.ByteSize		= 4;
	UartSendText(uart, "Long Access Ready\r\n");
	return(iResult);
}

//============================================================================
//	アスキー表示トグル
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdAscii(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM iResult = MON_INIT;

	if (MonParamIsData(Line))
	{
		Line->Option.Bit.Asc	= MonGetHexParam(Line);
	}
	else
	{
		Line->Option.Bit.Asc	= !Line->Option.Bit.Asc;
	}

	if (Line->Option.Bit.Asc)
	{
		UartSendText(uart, "Ascii is ON\r\n");
	}
	else
	{
		UartSendText(uart, "Ascii is OFF\r\n");
	}
	return(iResult);
}

//============================================================================
//	アスキー表示トグル
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdReadBack(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM iResult = MON_INIT;

	if (MonParamIsData(Line))
	{
		Line->Option.Bit.ReadBack	= MonGetHexParam(Line);
	}
	else
	{
		Line->Option.Bit.ReadBack	= !Line->Option.Bit.ReadBack;
	}

	if (Line->Option.Bit.ReadBack)
	{
		UartSendText(uart, "read back is ON\r\n");
	}
	else
	{
		UartSendText(uart, "read back is OFF\r\n");
	}
	return(iResult);
}

//============================================================================
//	コマンドヘルプ
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdHelp(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM iResult = MON_INIT;
	
	return(iResult);
}

//============================================================================
//	何もしないで戻る
//----------------------------------------------------------------------------
//
//============================================================================
MON_SEQ_ENUM MonCmdNop(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM iResult = MON_INIT;
	
	return(iResult);
}
	