/*****************************************************************************
	シリアル通信
------------------------------------------------------------------------------

*****************************************************************************/
#include <machine.h>
#include "uart.h"
#include "vect.h"

#define SEND_BUFF_SIZE    80
#define RECV_BUFF_SIZE    80

static UART_TYPE 	m_uart0;
static FIFO_TYPE 	m_uart0_SendFifo;
static FIFO_TYPE 	m_uart0_RecvFifo;
static char 		m_SendBuff0[SEND_BUFF_SIZE];
static char 		m_RecvBuff0[RECV_BUFF_SIZE];

UART_TYPE *	uart0	= &m_uart0;

//============================================================================
//	初期化
//----------------------------------------------------------------------------
// call
//	uart	ﾊﾝﾄﾞﾗ
// return
//	ハンドラ
// ex)
//	uart = Uart0Init(&uart_t);
//============================================================================

//clk  120MHz/8 = 15MHz
UART_TYPE *Uart0Init(void)
{
	UART_TYPE               *uart 		= &m_uart0;
volatile int i;
	

	SYSTEM.PRCR.WORD               		= 0xa502;
	SYSTEM.MSTPCRB.BIT.MSTPB31			= OFF;

	PORT3.PMR.BIT.B2 					= 1;				// Use P30 for RXD1
	PORT3.PMR.BIT.B3 					= 1;				// Use P26 for TXD1
	
	uart->sci     		= &SCI0;
	uart->Send 			= FifoInit(&m_uart0_SendFifo, m_SendBuff0, SEND_BUFF_SIZE);
	uart->Recv 			= FifoInit(&m_uart0_RecvFifo, m_RecvBuff0, RECV_BUFF_SIZE);

	SCI0.SCR.BYTE 		= 0x00;
	SCI0.SMR.BYTE 		= 0x00;

	// n = (PCLKx10^6/(64x2^(2n-1)xB) - 1
	// n = (12000000/(64/2x9600)-1 = 38.0625
	SCI0.BRR 			= 38;      //38 : 9600bps
	SCI0.SCR.BYTE 		= 0x50;
	
	SCI0.BRR = 49;
	
	ICU.IPR[214].BIT.IPR      	= 3;		//RXI0
	ICU.IPR[215].BIT.IPR      	= 3;		//TXI0
	ICU.IPR[216].BIT.IPR      	= 3;		//TEI0
	ICU.IER[0x1A].BIT.IEN6		= ON;		
	ICU.IER[0x1A].BIT.IEN7		= ON;	
	ICU.IER[0x1B].BIT.IEN0		= ON;
	
	return(uart);
}

//============================================================================
//	1文字送信
//----------------------------------------------------------------------------
// call
//	uart	ﾊﾝﾄﾞﾗ
// return
//	true	成功
//	false	バッファが一杯
//============================================================================
bool UartSendByte(UART_TYPE *uart, char Data)
{
	bool	bResult;
	bResult = FifoWriteByte(uart->Send, Data);
	SCI0.SCR.BIT.TIE  	 	= ON;
	SCI0.SCR.BIT.TE			= ON;
	return(bResult);
}

//============================================================================
//	テキスト送信
//----------------------------------------------------------------------------
// call
//	uart	ﾊﾝﾄﾞﾗ
//	Text	送信する文字列 (\0で終わる)
// return
//	true	成功
//	false	処理中
// caution
//	バッファ数以上の文字は送信できない
//	処理中の送信文字Textは適用されない
//============================================================================
bool UartSendText(UART_TYPE *uart, char *Text)
{
	bool 		bResult 	= false;
	static char 	*cText	 	= null;
	if (cText == null)
	{
		cText = FifoWriteText(uart->Send, Text);
		SCI0.SCR.BIT.TIE   	= ON;
		SCI0.SCR.BIT.TE		= ON;
	}
	else
	{
		cText = FifoWriteText(uart->Send, cText);
	}

	if (cText == null) bResult = true;
	return(bResult);
}

//============================================================================
//	受信バッファから１文字読み出し
//----------------------------------------------------------------------------
// call
//	uart	ﾊﾝﾄﾞﾗ
// return
//	データ
//	-1	受信文字なし
//============================================================================
int UartRecvByte(UART_TYPE *uart)
{
	if (SCI0.SSR.BYTE)
	{
		SCI0.SSR.BYTE 	= 0xc0;
	}
	return FifoReadByte(uart->Recv);
}

//============================================================================
//	割込み処理
//============================================================================
#pragma section IntPRG

// SCI0 RXI0
void Excep_SCI0_RXI0(void)
{ 
	if (FifoWriteByte(uart0->Recv, SCI0.RDR)==false)
	{
		uart0->Error.Bit.Overflow = true;
	}
}

// SCI0 TXI0
void Excep_SCI0_TXI0(void)
{
	if (FifoIsEmpty(uart0->Send)==true)
	{	
		SCI0.SCR.BIT.TIE		= OFF;
		SCI0.SCR.BIT.TEIE		= ON;
	}
	else
	{
		SCI0.TDR = FifoReadByte(uart0->Send);
	}
}

void Excep_SCI0_TEI0(void)
{
	SCI0.SCR.BIT.TEIE			= OFF;
	SCI0.SCR.BIT.TE				= OFF;
}
