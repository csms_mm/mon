/*****************************************************************************
	j^
------------------------------------------------------------------------------

*****************************************************************************/
#include "mon.h"
#include "charcode.h"


LINEBUFF_TYPE m_LineBuff;

void MonSendPrompt(UART_TYPE *uart);
void LineBuffClear(LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonGetLine(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonGetCommand(const MON_COMMAND_LIST_TYPE *CmdList,  LINEBUFF_TYPE *Line);



void Mon(void)
{
	MON_SEQ_ENUM		MonSeq		= MON_INIT;
	UART_TYPE 			*uart 		= Uart0Init();
	LINEBUFF_TYPE   	*Line	 	= &m_LineBuff;
	long Count = 0;
	
	UartSendText(uart, "RD-290 mon v0.00\r\n");
	//UartSendText(uart, "RD\r\n");
	
	LineBuffClear(Line);
	while (MonSeq != MON_EXIT)
	{
		Count ++;
		if (Count > 500000)
		{
			LED15 =  !LED15;
			Count = 0;
		}
		Line->Key = UartRecvByte(uart);
		switch(MonSeq)
		{
		case MON_INIT:
			MonSendPrompt(uart);
			Line->Pos				= 0;
			MonSeq = MON_OPERATION;
			break;
		case MON_OPERATION:
			MonSeq = MonGetLine(uart, Line);
			break;
		case MON_RUN:
			MonSeq = Line->Func(uart, Line);
			break;
		default:
			UartSendText(uart, "Prg Error\r\n");
			MonSeq = MON_INIT;
			break;
		}
	}
}

void MonSendPrompt(UART_TYPE *uart)
{
	UartSendText(uart, "> ");
}

//============================================================================
//		
//----------------------------------------------------------------------------
//============================================================================
MON_SEQ_ENUM MonGetLine(UART_TYPE *uart, LINEBUFF_TYPE *Line)
{
	MON_SEQ_ENUM    iResult	= MON_OPERATION;
	int		i;
	#ifdef EXTKEYCODE
	// g£R[hæ¾
	if ( Line->Option.Bit.Esc)
	{
		switch(Line->Key)
		{
		case -1:
			//ESCÌ®ì
			Line->Key					= ccESC;
			break;
		case 'A':	// 'ª'
			Line->Key					= ccUP;
			break;
		case 'B':	// '«'
			Line->Key					= ccDOWN;
			break;
		case 'C':	// '©'
			Line->Key					= ccLEFT;
			break;
		case 'D':	// '¨'
			Line->Key					= ccRIGHT;
			break;
		default:
			break;
		}
		Line->Option.Bit.Esc		= OFF;	
	}
	else
	{
	#endif
		if ( Line->Key < ' ') 
		{
			switch(Line->Key)
			{
			case ccCR:
				UartSendText(uart, CRLF);
				Line->LineBuff[Line->Pos] 	= 0;
				iResult = MonGetCommand(MonCommandList, Line);
				if (iResult == MON_INIT)
				{
					UartSendText(uart, "command not found\r\n");
				}
				break;
			case ccESC:
				Line->WaitCount				= 0;
				Line->Option.Bit.Esc        = ON;
				break;
			case ccDEL:
				break;
			case ccBS:	//P¶í
				if (Line->Pos > 0)
				{
					Line->Pos --;
					UartSendText(uart, ESC_CURSOL_LEFT ESC_ERASE_3);
				}
				break;
			case ccCTRL_C:
				for (i = 0; i < Line->Pos; i ++)
				{
					Line->CopyBuff[i] = Line->LineBuff[i];
				}
				Line->CopyBuff[Line->Pos] = 0;
				break;
			case ccCTRL_V:
				for (i = 0; i < sizeof(Line->LineBuff); i ++)
				{
					Line->LineBuff[i] = Line->CopyBuff[i];
					if (Line->CopyBuff[i]==0) 
					{
						break;
					}
				}
				Line->Pos = i;
				UartSendText(uart, ESC_ERASE_LINE);
				MonSendPrompt(uart);
				UartSendText(uart, Line->LineBuff);
				break;		
			case ccCTRL_Q:
				Line->Pos	= 0;
				UartSendText(uart, CRLF);
				iResult 	= MON_EXIT;
				break;
			}
		}
		else
		{
			if (Line->Pos < (sizeof(Line->LineBuff)-1))
			{
				Line->LineBuff[Line->Pos] 	= Line->Key;
				Line->Pos ++;
				UartSendByte(uart, Line->Key);
			}
		}
	#ifdef EXTKEYCODE
	}
	#endif
	return(iResult);
}

