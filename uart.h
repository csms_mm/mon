/***********************************************************************
	シリアル通信
------------------------------------------------------------------------

***********************************************************************/
#ifndef _UART_H_
#include "iodefine.h"
#include "mydefine.h"
#include "fifo.h"

typedef volatile struct st_sci0 __evenaccess  SCI_TYPE;

typedef struct
{
	SCI_TYPE		  	*sci;
	FIFO_TYPE 			*Recv;
	FIFO_TYPE 			*Send;
	union
	{
		int			Word;
		struct
		{
			int		Overflow   :1;
			int		Underflow  :1;
			int 		OverRun    :1;
			int		Framing    :1;
		} Bit;
	} Error;
} UART_TYPE;


UART_TYPE *Uart0Init(void);
bool UartSendByte(UART_TYPE *uart, char Data);
bool UartSendText(UART_TYPE *uart, char *Text);
int UartRecvByte(UART_TYPE *uart);


#define _UART_H_
#endif