/***********************************************************************/
/*                                                                     */
/*  FILE        :charcode.h                                            */
/*  DATE        :Mon, Oct 28, 2002                                     */
/*  DESCRIPTION :charctor code define                                  */
/*                                                                     */
/*  Copyright (C) all right reserved                                   */
/*                                                                     */
/***********************************************************************/
#ifndef _CAHRCODE_H

//機能コード
#define ccNUL					0x00			//ヌル文字
#define ccSOH					0x01			//ヘッダ開始
#define ccSTX					0x02			//テキスト開始
#define ccETX					0x03			//テキスト終了
#define ccEOT					0x04			//転送終了
#define ccENQ					0x05			//照会
#define ccACK					0x06			//受信ＯＫ
#define ccBEL					0x07			//警告
#define ccBS					0x08			//後退
#define ccHT					0x09			//水平タブ
#define ccLF					0x0A			//改行
#define ccVT					0x0B			//垂直タブ
#define ccFF					0x0C			//改頁
#define ccCR					0x0D			//復帰
#define ccSO					0x0E			//シフトアウト
#define ccSI					0x0F			//シフトイン
#define ccDLE					0x10			//データリンクエスケープ
#define ccDC1					0x11			//装置制御１
#define ccDC2					0x12			//装置制御２
#define ccDC3					0x13			//装置制御３
#define ccDC4					0x14			//装置制御４
#define ccNAK					0x15			//受信失敗
#define ccSYN					0x16			//同期
#define ccETB					0x17			//転送ブロック終了
#define ccCAN					0x18			//取消
#define ccEM					0x19			//メディア終了
#define ccSUB					0x1A			//置換
#define ccESC					0x1B			//エスケープ
#define ccFS					0x1C			//フォーム区切り
#define ccGS					0x1D			//グループ区切り
#define ccRS					0x1E			//レコード区切り
#define ccUS					0x1F			//ユニット区切り

//キーコード
#define ccCTRL_A				0x01
#define ccCTRL_B				0x02
#define ccCTRL_C				0x03			//コピー
#define ccCTRL_D				0x04
#define ccCTRL_E				0x05
#define ccCTRL_F				0x06			//検索
#define ccCTRL_G				0x07
#define ccCTRL_H				0x08
#define ccCTRL_I				0x09
#define ccCTRL_J				0x0A
#define ccCTRL_K				0x0B
#define ccCTRL_L				0x0C
#define ccCTRL_M				0x0D
#define ccCTRL_N				0x0E
#define ccCTRL_O				0x0F
#define ccCTRL_P				0x10
#define ccCTRL_Q				0x11
#define ccCTRL_R				0x12
#define ccCTRL_S				0x13
#define ccCTRL_T				0x14
#define ccCTRL_U				0x15
#define ccCTRL_V				0x16			//ペースト
#define ccCTRL_W				0x17
#define ccCTRL_X				0x18			//カット
#define ccCTRL_Z				0x19

//拡張キーコード
#define ccINS					0x5200
#define ccDEL					0x5300
#define ccUP					0x4800
#define ccDOWN					0x5000
#define ccRIGHT					0x4d00
#define ccLEFT					0x4b00
#define ccHOME					0x4700
#define ccPGUP					0x4900
#define ccPGDN					0x5100
#define ccEND					0x4f00
#define ccF1					0x3b00
#define ccF2					0x3c00
#define ccF3					0x3d00
#define ccF4					0x3e00
#define ccF5					0x3f00
#define ccF6					0x4000
#define ccF7					0x4100
#define ccF8					0x4200
#define ccF9					0x4300
#define ccF10					0x4400
#define ccF11					0x8500
#define ccF12					0x8600
#define ccSHFT_1				0x5400
#define ccSHFT_2				0x5500
#define ccSHFT_3				0x5600
#define ccSHFT_4				0x5700
#define ccSHFT_5				0x5800
#define ccSHFT_6				0x5900
#define ccSHFT_7				0x5a00
#define ccSHFT_8				0x5b00
#define ccSHFT_9				0x5c00
#define ccSHFT_10				0x5d00
#define ccSHFT_11				0x8700
#define ccSHFT_12				0x8800
#define ccCTRL_F1				0x5e00
#define ccCTRL_F2				0x5f00
#define ccCTRL_F3				0x6000
#define ccCTRL_F4				0x6100
#define ccCTRL_F5				0x6200
#define ccCTRL_F6				0x6300
#define ccCTRL_F7				0x6400
#define ccCTRL_F8				0x6500
#define ccCTRL_F9				0x6600
#define ccCTRL_F10				0x6700
#define ccCTRL_F11				0x6800
#define ccCTRL_F12				0x6900

/*****    エスケープシーケンス    *****/
#define	ESC_CLR					"\x1b[2J"		//画面クリア
#define	ESC_CURSOL_OFF			"\x1b[>5h"
#define	ESC_CURSOL_ON			"\x1b[>5l"
#define	ESC_CURSOL_HOME			"\x1b1;1H"		//ホームポジションへ
#define	ESC_CURSOL_SAVE			"\x1b[s"		//
#define	ESC_CURSOL_LOAD			"\x1b[u"
#define	ESC_CURSOL_UP			"\x1b[A"
#define	ESC_CURSOL_DOWN			"\x1b[B"
#define	ESC_CURSOL_RIGHT		"\x1b[C"
#define	ESC_CURSOL_LEFT			"\x1b[D"
#define	ESC_ERASE_COLUMNEND		"\x1b[K"		//ｶｰｿﾙ以降の行削除

//仮名称
#define	ESC_ERASE_1				"\x1b[0J"	//画面のカーソル以降削除
#define	ESC_ERASE_2				"\x1b[1J"	//画面のカーソルまで削除
#define	ESC_ERASE_3				"\x1b[0K"	//行のカーソル以降削除
#define	ESC_ERASE_4				"\x1b[1K"	//行のカーソルまで削除
#define	ESC_ERASE_LINE			"\x1b[2K"	//行の削除
//#define	ESC_CD7				"\x1b[nM"	//n行削除
//#define	ESC_CD8				"\x1b[nL"	//n行挿入


#define	ESC_ATTR_RES			"\x1b[0m"
#define	ESC_BOLD				"\x1b[1m"
#define	ESC_UNDER				"\x1b[4m"
#define	ESC_BLINK				"\x1b[5m"		//文字の点滅
#define	ESC_REVERSE				"\x1b[7m"
#define	ESC_SECRET				"\x1b[8m"

#define	ESC_COLOR_NORMAL		"\x1b[39m"
#define	ESC_COLOR_BLACK			"\x1b[30m"
#define	ESC_COLOR_GRAY			"\x1b[30m"
#define	ESC_COLOR_RED			"\x1b[31m"
#define	ESC_COLOR_GREEN			"\x1b[32m"
#define	ESC_COLOR_YELLOW		"\x1b[33m"
#define	ESC_COLOR_BROWN			"\x1b[33m"
#define	ESC_COLOR_BLUE			"\x1b[34m"
#define	ESC_COLOR_PERPLE		"\x1b[35m"
#define	ESC_COLOR_CYAN			"\x1b[36m"
#define	ESC_COLOR_WHITE			"\x1b[37m"
#define	ESC_COLOR_SET_SECRET	"\x1b[16m"
#define	ESC_COLOR_SET_RED		"\x1b[17m"
#define	ESC_COLOR_SET_GREEN		"\x1b[20m"
#define	ESC_COLOR_SET_BROWN		"\x1b[21m"
#define	ESC_COLOR_SET_BLUE		"\x1b[18m"
#define	ESC_COLOR_SET_PERPLE	"\x1b[19m"
#define	ESC_COLOR_SET_CYAN		"\x1b[22m"
#define	ESC_COLOR_SET_WHITE		"\x1b[23m"
#define	ESC_COLOR_D_BLACK		"\x1b[0;30m"
#define	ESC_COLOR_D_RED			"\x1b[0;31m"
#define	ESC_COLOR_D_GREEN		"\x1b[0;32m"
#define	ESC_COLOR_D_YELLOW		"\x1b[0;33m"
#define	ESC_COLOR_D_BROWN		"\x1b[0;33m"
#define	ESC_COLOR_D_BLUE		"\x1b[0;34m"
#define	ESC_COLOR_D_PERPLE		"\x1b[0;35m"
#define	ESC_COLOR_D_CYAN		"\x1b[0;36m"
#define	ESC_COLOR_D_WHITE		"\x1b[0;37m"
#define	ESC_COLOR_L_GRAY		"\x1b[1;30m"
#define	ESC_COLOR_L_RED			"\x1b[1;31m"
#define	ESC_COLOR_L_GREEN		"\x1b[1;32m"
#define	ESC_COLOR_L_YELLOW		"\x1b[1;33m"
#define	ESC_COLOR_L_BLUE		"\x1b[1;34m"
#define	ESC_COLOR_L_PERPLE		"\x1b[1;35m"
#define	ESC_COLOR_L_CYAN		"\x1b[1;36m"
#define	ESC_COLOR_L_WHITE		"\x1b[1;37m"
#define	ESC_BG_BLACK			"\x1b[40m"
#define	ESC_BG_RED				"\x1b[41m"
#define	ESC_BG_GREEN			"\x1b[42m"
#define	ESC_BG_BROWN			"\x1b[43m"
#define	ESC_BG_BLUE				"\x1b[44m"
#define	ESC_BG_PERPLE			"\x1b[45m"
#define	ESC_BG_CYAN				"\x1b[46m"
#define	ESC_BG_GRAY				"\x1b[47m"
#define	ESC_BG_WHITE			"\x1b[1;47m"
#define	ESC_BG_NORMAL			"\x1b[49m"

#define	CRLF					"\r\n"

//	"\x1b[x;yH"							//x,yにｶｰｿﾙ移動
//	"\x1b[nA"							//n行上へｶｰｿﾙ移動
//	"\x1b[nB"							//n行下へｶｰｿﾙ移動
//	"\x1b[nC"							//n桁右へｶｰｿﾙ移動
//	"\x1b[nD"							//n桁左へｶｰｿﾙ移動


#define		_CAHRCODE_H
#endif


