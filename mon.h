/*****************************************************************************
	モニタ
------------------------------------------------------------------------------

*****************************************************************************/
#ifndef _MON_H_
#include "hardware.h"

typedef enum 
{
	MON_INIT,
	MON_OPERATION,
	MON_RUN,
	MON_EXIT
} MON_SEQ_ENUM;

typedef union
{
	int 		Word;
	struct
	{
		int     	Loop  		: 1;
		int			Esc   		: 1;
		int			ReadBack    : 1;		//リードバック
		int			Asc			: 1;		//ASCII コード出力
		int			ByteSize  	: 3;
		int			DispAdr		: 1;		//ｱﾄﾞﾚｽ表示
		int     	wk    :8;
	} Bit;
} MON_OPTION_TYPE;

typedef struct 
{
	//input
	char 			LineBuff[80];
	char			CopyBuff[80];
	int				Key;			//
	char			*Cmd;
	int				Pos;
	
	//command
	long			Adr;
	long			Data;
	long			Count;
	
	//command option
	long			AdrAdd;
	long			ActAdr;
	long			DataAdd;
	long			ActData;
	
	int				WaitCount;
	MON_SEQ_ENUM	Seq;
	MON_SEQ_ENUM    (* Func)();
	MON_OPTION_TYPE Option;
} LINEBUFF_TYPE;

typedef struct
{
	const	char				*Name;
	MON_SEQ_ENUM				(*Func)(UART_TYPE *uart, LINEBUFF_TYPE *Line);
	const	char				*Syntax;
	const	char				*Comment;
} MON_COMMAND_LIST_TYPE;

extern const MON_COMMAND_LIST_TYPE MonCommandList[];

void Mon(void);
void LineBuffClear(LINEBUFF_TYPE *Line);
void MonSendHex(UART_TYPE *uart, long Data, int iColSize);
void MonSendData(UART_TYPE *uart, LINEBUFF_TYPE *Line, long Data);

MON_SEQ_ENUM MonCmdPI(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdPO(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdByte(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdWord(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdLong(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdAscii(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdReadBack(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdHelp(UART_TYPE *uart, LINEBUFF_TYPE *Line);
MON_SEQ_ENUM MonCmdNop(UART_TYPE *uart, LINEBUFF_TYPE *Line);

#define _MON_H_
#endif

