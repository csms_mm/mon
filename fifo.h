/***********************************************************************
	FIFO
------------------------------------------------------------------------

***********************************************************************/
#ifndef _FIFO_H_
#include "mydefine.h"

typedef struct
{
	char   	*Buff;
	int     Size;
	int 	ReadPos;
	int	WritePos;
} FIFO_TYPE;

FIFO_TYPE* FifoInit(FIFO_TYPE *fifo, char *Buff, int Size);
void FifoClear(FIFO_TYPE *fifo);

int FifoReadByte(FIFO_TYPE *fifo);
bool FifoWriteByte(FIFO_TYPE *fifo, char Data);
int FifoNextPos(FIFO_TYPE *fifo, int NowPos);

bool FifoIsFull(FIFO_TYPE *fifo);
char* FifoWriteText(FIFO_TYPE* fifo, char* Text);
int FifoWriteData(FIFO_TYPE *fifo, char *Data, int Size);


#define _FIFO_H_
#endif
